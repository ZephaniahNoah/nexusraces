# Nexus Races

A races/classes mod for Minecraft

You may copy this code and modify it and distribute it as you please. Just please remember to give credit to it's creators. See the [mcmod.info](https://gitlab.com/ZephaniahNoah/nexusraces/-/blob/master/src/main/resources/mcmod.info) file for the authors list.
This mod was made for multiplayer. As such, I never bothered to make some things work in singleplayer.
It depends on ElectroBob's Wizardry. Put the mod into the libs folder. Change the name of the mod to ElectroblobsWizardry.jar then gradle should be able to build the mod properly.

