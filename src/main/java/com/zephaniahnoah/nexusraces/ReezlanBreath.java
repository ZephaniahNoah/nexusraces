package com.zephaniahnoah.nexusraces;

import net.minecraft.entity.player.EntityPlayer;

public class ReezlanBreath {
	
	private double breath;
	public EntityPlayer reezlan;
	private static final double maxBreath = 240;
	
	public ReezlanBreath(EntityPlayer p) {
		reezlan = p;
		breath = maxBreath;
	}
	
	public void drown() {
		breath-=1;
		reezlan.setAir((int) ((breath/maxBreath)*300));
	}
}
