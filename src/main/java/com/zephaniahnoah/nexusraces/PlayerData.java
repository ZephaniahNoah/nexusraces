package com.zephaniahnoah.nexusraces;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.entity.player.EntityPlayer;

public class PlayerData {
	
	private static List<String[]> playerData = new ArrayList<String[]>();
	private static File file;

    public static void init(File f) {
    	file = f;
    	BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(file));
			
			String line = reader.readLine();
			
			while (line != null) {
				// Parse time
				playerData.add(line.split("="));
				
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

    public static void setClass(EntityPlayer p, PlayerClass c) {
    	Main.resetEffects(p);
    	boolean noClass = true;
    	for(String[] data : playerData) {
    		if(data[0].equals(p.getName())) {
    			data[1]=c.name();
    			noClass = false;
    		}
    	}
    	
    	if(noClass) {
			playerData.add(new String[] {p.getName(),c.name()});
		}
    	
    	// Save
    	BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(file));
			
			for(String[] data : playerData) {
				writer.write(data[0]+"="+data[1]);
				writer.newLine();
			}
			
			//writer.append(p.getName()+"="+c.name());
			
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

    public static PlayerClass stringToClass(String name) {
        for (PlayerClass playerClass : PlayerClass.values()) {
            // TODO: == OR .Equals ???
            if (name.equals(playerClass.name())) {
            	//System.out.println(playerClass);
                return playerClass;
            }
        }
        return PlayerClass.None;
    }
    
    public static PlayerClass getPlayerClass(EntityPlayer p) {
    	for(String[] data : playerData) {
    		if(data[0].equals(p.getName())) {
    			return stringToClass(data[1]);
    		}
    	}
    	return PlayerClass.None;
    }
}