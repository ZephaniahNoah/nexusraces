package com.zephaniahnoah.nexusraces.gui;

import com.zephaniahnoah.nexusraces.Config;
import com.zephaniahnoah.nexusraces.Main;
import com.zephaniahnoah.nexusraces.PlayerClass;
import com.zephaniahnoah.nexusraces.PlayerData;
import com.zephaniahnoah.nexusraces.packets.SendSetPlayerClass;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentString;

public class GuiChooseClass extends GuiScreen {

    public static final ResourceLocation GUI_TEXTURES = new ResourceLocation(Main.MODID, "textures/gui/choose_class.png");
    private Button[] buttons = new Button[11];
    private EntityPlayer player;
    
    public GuiChooseClass(EntityPlayer p) {
    	player = p;
    }
    
    public static void Instantiate(EntityPlayer p) {
    	if(p.isCreative() || PlayerData.getPlayerClass(p) == PlayerClass.None) {
    		Minecraft.getMinecraft().displayGuiScreen(new GuiChooseClass(p));
    	} else {
    		p.sendMessage(new TextComponentString("You must be in creative mode to change your class."));
    	}
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
    	drawDefaultBackground();
        mc.getTextureManager().bindTexture(GUI_TEXTURES);
        GlStateManager.pushMatrix();
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        GlStateManager.enableBlend();
        GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA,
        GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE,
        GlStateManager.DestFactor.ZERO);
        GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);

        int x = (this.width - 229) / 2;
        int y = (this.height - 219) / 2;

        this.drawTexturedModalRect(x, y, 0, 0, 229, 219);

        GlStateManager.popMatrix();
        
        boolean found = false;
        
        for(int i = 0; i<11;i++) {
        	buttons[i].drawButton(mc, mouseX, mouseY, partialTicks);
        	this.zLevel = 100;
        	this.drawTexturedModalRect(buttons[i].x, buttons[i].y, i*22, 219, 22, 22);
        	this.zLevel = 0;
        	if(buttons[i].textureY == 22) {
        		found = true;
        		for(int j=0;j<Config.classDescriptions[i].length;j++) {
        			fontRenderer.drawStringWithShadow(j == 0 ? "§l" + Config.classDescriptions[i][j] : Config.classDescriptions[i][j], x + 15, y + 74 + (j*10), 0xffffff);
        		}
        	}
        }
        
        if (!found){
    		this.drawCenteredString(fontRenderer, "§lChoose a class!", width/2, height/2 + 23, 0xffffff);
    	}
        
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);

        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    @Override
    public boolean doesGuiPauseGame()
    {
        return false;
    }
    
    @Override
    public void initGui(){
        super.initGui();
        int x = 88;
        int y = 99;
        for(int i = 0; i<11;i++) {
        	if(i==7) {
        		y = 74;
        		x = 231;
        	}
        	buttons[i] = new Button(i, this.width / 2 - x + (26 * i), this.height / 2 - y);
        	this.buttonList.add(buttons[i]);
        }
    }

    @Override
    public void actionPerformed(GuiButton button) {
    	PlayerClass pc = PlayerClass.values()[button.id];
        Main.INSTANCE.sendToServer(new SendSetPlayerClass(pc));
        PlayerData.setClass(player, pc);
        if(pc == PlayerClass.Elf) {
        	mc.displayGuiScreen(new GuiSpellBook(player));
        } else {
        	mc.displayGuiScreen(null);
        }
    }
}