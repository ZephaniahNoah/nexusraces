package com.zephaniahnoah.nexusraces.gui;

import java.util.ArrayList;
import java.util.List;

import com.zephaniahnoah.nexusraces.Config;
import com.zephaniahnoah.nexusraces.Main;
import com.zephaniahnoah.nexusraces.packets.SendSpellBook;

import electroblob.wizardry.registry.WizardryItems;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class GuiSpellBook extends GuiScreen{
	
    public static final ResourceLocation GUI_TEXTURES = new ResourceLocation(Main.MODID, "textures/gui/choose_book.png");
    private Button[] buttons;
    private EntityPlayer player;
    private final List<Integer> books = new ArrayList<Integer>(5);
    
    public GuiSpellBook(EntityPlayer p) {
    	player = p;
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
    	drawDefaultBackground();
        mc.getTextureManager().bindTexture(GUI_TEXTURES);
        GlStateManager.pushMatrix();
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        GlStateManager.enableBlend();
        GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA,
        GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE,
        GlStateManager.DestFactor.ZERO);
        GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);

        int x = (this.width - 229) / 2;
        int y = (this.height - 219) / 2;

        this.drawTexturedModalRect(x, y, 0, 0, 229, 219);

        GlStateManager.popMatrix();

        int rows = Config.books.size() / 7;

        if(rows % 7 > 0) {
        	rows++;
        }
        
        int rowLength = 7;
        
        String message = "§6Choose five books!";
        
        int a=0;
        int b=0;
        for(int i = 0;i<Config.books.size();i++) {
        	if(a==7) {
        		a=0;
        		b++;
        	}
        	if(b+1 == rows) {
        		rowLength = Config.books.size() % 7;
        	}
			this.itemRender.renderItemIntoGUI(new ItemStack(WizardryItems.spell_book), (this.width / 2 - (rowLength*25)/2) + a*25 + 3, this.height / 2 - ((rows*25)/2) + b*25 + 3);
        	a++;
        	if(buttons[i].textureY == 22) {
        		message = Config.books.get(i).getTooltip(player, ITooltipFlag.TooltipFlags.NORMAL).get(1);
        	}
        }
        
    	this.drawCenteredString(electroblob.wizardry.client.ClientProxy.mixedFontRenderer, message, width/2, height/2 - 100, 0xffffff);
    	
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        
        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    @Override
    public boolean doesGuiPauseGame()
    {
        return false;
    }
    
    @Override
    public void initGui(){
        super.initGui();
        
        buttons = new Button[Config.books.size()];
        
        int rows = Config.books.size() / 7;
        
        if(rows % 7 > 0) {
        	rows++;
        }
        
        int rowLength = 7;
        
        int x=0;
        int y=0;
        for(int i = 0;i<Config.books.size();i++) {
        	if(x==7) {
        		x=0;
        		y++;
        	}
        	if(y+1 == rows) {
        		rowLength = Config.books.size() % 7;
        	}
        	buttons[i] = new Button(i, (this.width / 2 - (rowLength*25)/2) + x*25, this.height / 2 - ((rows*25)/2) + y*25);
        	this.buttonList.add(buttons[i]);
        	x++;
        }
    }

    @Override
    public void actionPerformed(GuiButton button) {
    	((Button)button).textureY=0;
    	books.add(button.id);
		this.buttonList.remove(button);
    	if(books.size()>4) {
    		for(int i = 0; i < 5; i ++) {
    			Main.INSTANCE.sendToServer(new SendSpellBook(Config.books.get(books.get(i)).getMetadata()));
    		}
    		mc.displayGuiScreen(null);
    	}
    }
}
