package com.zephaniahnoah.nexusraces.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.util.ResourceLocation;

public class Button extends GuiButton {

    final ResourceLocation texture = GuiChooseClass.GUI_TEXTURES;

    public static int size = 22;
    private int textureX = 230;
    public int textureY = 0;

    public Button(int buttonId, int x, int y) {
        super(buttonId, x, y, size, size, "");
    }

    @Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
        if (visible) {
            mc.renderEngine.bindTexture(texture);
            if (mouseX >= x && mouseX <= x + size && mouseY >= y && mouseY <= y + size) {
            	textureY = size;
            } else {
            	textureY = 0;
            }
            drawTexturedModalRect(x, y, textureX, textureY, size, size);
        }
    }
}
