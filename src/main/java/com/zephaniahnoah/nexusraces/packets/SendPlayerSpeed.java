package com.zephaniahnoah.nexusraces.packets;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class SendPlayerSpeed implements IMessage {

	public SendPlayerSpeed(){}

	public double toSend;

	public SendPlayerSpeed(double speed) {	
		this.toSend = speed;
	}

	@Override public void toBytes(ByteBuf buf) {
		buf.writeDouble(toSend);
	}

	@Override public void fromBytes(ByteBuf buf) {
		toSend = buf.readDouble();
	}
}