package com.zephaniahnoah.nexusraces.packets;

import java.util.UUID;

import com.zephaniahnoah.nexusraces.Main;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PlayerSpeedPacketHandler implements IMessageHandler<SendPlayerSpeed, IMessage> {

	static AttributeModifier MY_CUSTOM_MODIFIER;

	@Override
	public IMessage onMessage(SendPlayerSpeed message, MessageContext ctx) {

		double speed = message.toSend;

		EntityPlayerMP p = ctx.getServerHandler().player;

		p.getServerWorld().addScheduledTask(() -> {
			setPlayerSpeed(p, speed);
		});

		return null;
	}

	// @SideOnly(Side.SERVER)
	public static void setPlayerSpeed(EntityPlayer p, double d) {
		MY_CUSTOM_MODIFIER = new AttributeModifier(UUID.fromString("FABD628F-0E1B-727F-6560-012DDB8FD973"),
				Main.MODID + ".playerSpeed", d, 0);
		if (p.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).hasModifier(MY_CUSTOM_MODIFIER)) {
			p.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).removeModifier(MY_CUSTOM_MODIFIER);
		} else {
			p.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).applyModifier(MY_CUSTOM_MODIFIER);
		}
	}
}