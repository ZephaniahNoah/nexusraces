package com.zephaniahnoah.nexusraces.packets;

import java.util.ArrayList;

import com.zephaniahnoah.nexusraces.Config;

import electroblob.wizardry.registry.WizardryItems;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BookListPacketHandler implements IMessageHandler<SendBookList, IMessage> {
	
	public static final int booksStart = 999999999;

	@Override
	@SideOnly(Side.CLIENT)
	public IMessage onMessage(SendBookList message, MessageContext ctx) {

	int book = message.toSend;

	if(book == booksStart) {
		Config.books = new ArrayList<ItemStack>();
	} else {
		Config.books.add(new ItemStack(WizardryItems.spell_book,1,book));
	}

	// No response packet
	return null;
	}
}