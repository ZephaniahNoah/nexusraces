package com.zephaniahnoah.nexusraces.packets;

import com.zephaniahnoah.nexusraces.PlayerClass;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class SendGetClass implements IMessage {
	// A default constructor is always required
	public SendGetClass(){}

	public int toSend;

	public SendGetClass(PlayerClass playerClass) {	
		this.toSend = playerClass.ordinal();	
	}

	@Override public void toBytes(ByteBuf buf) {
		buf.writeInt(toSend);
	}

	@Override public void fromBytes(ByteBuf buf) {
		toSend = buf.readInt();
	}
}