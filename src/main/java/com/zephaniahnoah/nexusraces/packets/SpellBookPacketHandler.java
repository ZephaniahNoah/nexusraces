package com.zephaniahnoah.nexusraces.packets;

import electroblob.wizardry.registry.WizardryItems;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SpellBookPacketHandler implements IMessageHandler<SendSpellBook, IMessage> {
		
	@Override
	public IMessage onMessage(SendSpellBook message, MessageContext ctx) {
	
	EntityPlayerMP p = ctx.getServerHandler().player;
	
	int bookID = message.toSend;
	
	p.getServerWorld().addScheduledTask(() -> {
		p.inventory.addItemStackToInventory(new ItemStack(WizardryItems.spell_book, 1, bookID));
	});
	// No response packet
	return null;
	}
}