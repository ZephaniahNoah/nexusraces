package com.zephaniahnoah.nexusraces.packets;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class SendSpellBook implements IMessage {
	// A default constructor is always required
	public SendSpellBook(){}

	public int toSend;

	public SendSpellBook(int bookID) {	
		this.toSend = bookID;	
	}

	@Override public void toBytes(ByteBuf buf) {
		buf.writeInt(toSend);
	}

	@Override public void fromBytes(ByteBuf buf) {
		toSend = buf.readInt();
	}
}