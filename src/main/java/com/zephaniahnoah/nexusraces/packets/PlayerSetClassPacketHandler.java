package com.zephaniahnoah.nexusraces.packets;

import com.zephaniahnoah.nexusraces.Main;
import com.zephaniahnoah.nexusraces.PlayerClass;
import com.zephaniahnoah.nexusraces.PlayerData;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PlayerSetClassPacketHandler implements IMessageHandler<SendSetPlayerClass, IMessage> {
		
	@Override
	public IMessage onMessage(SendSetPlayerClass message, MessageContext ctx) {
	
	EntityPlayerMP p = ctx.getServerHandler().player;
	
	int playerClass = message.toSend;
	
	p.getServerWorld().addScheduledTask(() -> {
		if (PlayerData.getPlayerClass(p) == PlayerClass.None || p.isCreative()) {
			PlayerData.setClass(p, PlayerClass.values()[playerClass]);
			
			ItemStack item = p.inventory.getStackInSlot(p.inventory.currentItem);
			if(item.getCount() > 1) {
				item.setCount(item.getCount() - 1);
			} else {
				p.inventory.removeStackFromSlot(p.inventory.currentItem);
			}
			
			Main.resetEffects(p);
			Main.updateEffects(p);
		} else {
			p.sendMessage(new TextComponentString("You don't have permission to change your class."));
		}
	});
	// No response packet
	return null;
	}
}