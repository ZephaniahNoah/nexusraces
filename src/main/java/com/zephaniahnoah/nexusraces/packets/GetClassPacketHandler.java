package com.zephaniahnoah.nexusraces.packets;

import com.zephaniahnoah.nexusraces.PlayerClass;
import com.zephaniahnoah.nexusraces.PlayerData;

import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class GetClassPacketHandler implements IMessageHandler<SendGetClass, IMessage> {
		
	@Override
	@SideOnly(Side.CLIENT)
	public IMessage onMessage(SendGetClass message, MessageContext ctx) {

	//Main.clientClass = PlayerClass.values()[message.toSend];
	
	PlayerData.setClass(Minecraft.getMinecraft().player, PlayerClass.values()[message.toSend]);

	// No response packet
	return null;
	}
}