package com.zephaniahnoah.nexusraces.items;

import com.zephaniahnoah.nexusraces.Main;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ClassItem extends ModItem {

	public ClassItem(String name) {
		super(name);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer p, EnumHand handIn){
		if(handIn==EnumHand.MAIN_HAND) {
			Main.selectClass(p);
		}
		return super.onItemRightClick(world, p, handIn);
	}
}
