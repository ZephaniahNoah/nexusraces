package com.zephaniahnoah.nexusraces.items;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.ArrayList;

import com.zephaniahnoah.nexusraces.Main;

@Mod.EventBusSubscriber(modid= Main.MODID)
public class ModItems {
	// TO ADD ITEMS
/*  First, create a private static final Item Variable, using the ClassBlock class
    Second, make a itemname.json file in assets.lakraces.models.item
    Third, add a line to the en_us.lang file in assets.lakraces.lang

 */
	public static final ArrayList<Item> ITEMS = new ArrayList<Item>();
	public static final Item blankSoulStone = new ModItem("blank_soul_stone");
	public static final Item cactusThatch = new ModItem("cactus_thatch");
	public static final ClassItem classItem = new ClassItem("choose_class");
	public static final Item commonSoulStone = new ModItem("common_soul_stone");
	public static final Item corruptHide = new ModItem("corrupt_hide");
	public static final Item crystalApple = new ModItem("crystal_apple");
	public static final Item fireChalk = new ModItem("fire_chalk");
	public static final Item fireCoke = new ModItem("fire_coke");
	public static final Item flawlessAlchemyOrb = new ModItem("flawless_alchemy_orb");
	public static final Item flawlessAmber = new ModItem("flawless_amber");
	public static final Item flawlessBlackQuartz = new ModItem("flawless_black_quartz");
	public static final Item flawlessCarbonite = new ModItem("flawless_carbonite");
	public static final Item flawlessDiamond = new ModItem("flawless_diamond");
	public static final Item flawlessEmerald = new ModItem("flawless_emerald");
	public static final Item flawlessGoldNugget = new ModItem("flawless_gold_nugget");
	public static final Item flawlessIronNugget = new ModItem("flawless_iron_nugget");
	public static final Item flawlessLapis = new ModItem("flawless_lapis");
	public static final Item flawlessMoonstone = new ModItem("flawless_moonstone");
	public static final Item flawlessNetherShard = new ModItem("flawless_nether_shard");
	public static final Item flawlessPrismarine = new ModItem("flawless_prismarine_crystal");
	public static final Item flawlessRedstone = new ModItem("flawless_redstone_crystal");
	public static final Item flawlessSunstone = new ModItem("flawless_sunstone");
	public static final Item forgedObsidian = new ModItem("forged_obsidian");
	public static final Item grandSoulStone = new ModItem("grand_soul_stone");
	public static final Item greaterSoulStone = new ModItem("greater_soul_stone");
	public static final Item motifAwakened = new ModItem("motif_awakened");
	public static final Item motifDraconian = new ModItem("motif_draconian");
	public static final Item motifDwarf = new ModItem("motif_dwarf");
	public static final Item motifElf = new ModItem("motif_elf");
	public static final Item motifGaian = new ModItem("motif_gaian");
	public static final Item motifHellBorne = new ModItem("motif_hell_borne");
	public static final Item motifHuman = new ModItem("motif_human");
	public static final Item motifIsorian = new ModItem("motif_isorian");
	public static final Item motifKalot = new ModItem("motif_kalot");
	public static final Item motifLupithian = new ModItem("motif_lupithian");
	public static final Item motifMarrillian = new ModItem("motif_marrillian");
	public static final Item motifReezlan = new ModItem("motif_reezlan");
	public static final Item motifTekkin = new ModItem("motif_tekkin");
	public static final Item motifTestificate = new ModItem("motif_testificate");
	public static final Item orangeMushroom = new ModItem("orange_mushroom");
	public static final Item pettySoulStone = new ModItem("petty_soul_stone");
	public static final Item roughTekkinite = new ModItem("rough_tekkinite");
	public static final Item salt = new ModItem("salt");
	public static final Item shardBlank = new ModItem("shard_blank");
	public static final Item shardDo = new ModItem("shard_do");
	public static final Item shardFa = new ModItem("shard_fa");
	public static final Item shardLa = new ModItem("shard_la");
	public static final Item shardMi = new ModItem("shard_mi");
	public static final Item shardRe = new ModItem("shard_re");
	public static final Item shardSo = new ModItem("shard_so");
	public static final Item shardTi = new ModItem("shard_ti");
	public static final Item snowBerries = new ModItem("snow_berries");
	public static final Item tekkiniteIngot = new ModItem("tekkinite_ingot");
	public static final Item tuningFork = new ModItem("tuning_fork");
	public static final Item voidCrystal = new ModItem("void_crystals");
	public static final Item voidHeart = new ModItem("void_heart");
	public static final Item voidPowder = new ModItem("void_powder");

	@SubscribeEvent
	public static void registerItems(RegistryEvent.Register<Item> event) {
		for(Item item : ITEMS) {
			item.setCreativeTab(Main.tab);
			event.getRegistry().register(item);
		}
	}

	@SideOnly(Side.CLIENT)
	@SubscribeEvent   // TODO: Can subscribeEvent and sideOnly????
	public static void registerModels(ModelRegistryEvent e) {
		for(Item item : ITEMS) {
			registerRender(item);
		}
  	}
  	private static void registerRender(Item item) {
		ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(item.getRegistryName(), "inventory"));
	}
}
