package com.zephaniahnoah.nexusraces.items;

import com.zephaniahnoah.nexusraces.Main;

import net.minecraft.item.Item;

public class ModItem extends Item {

    public ModItem(String name) {
        setRegistryName(name);
        setUnlocalizedName(Main.MODID + "." + name);
        ModItems.ITEMS.add(this);
    }
}
