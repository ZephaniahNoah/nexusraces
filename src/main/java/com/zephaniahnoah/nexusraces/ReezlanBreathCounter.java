package com.zephaniahnoah.nexusraces;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.entity.player.EntityPlayer;

public class ReezlanBreathCounter {
	
	private List<ReezlanBreath> reezlans = new ArrayList<ReezlanBreath>();
	private List<ReezlanBreath> toRemove = new ArrayList<ReezlanBreath>();
	
	public void add(EntityPlayer p){
		reezlans.add(new ReezlanBreath(p));
	}
	
	public boolean has(EntityPlayer p) {
		for(ReezlanBreath b : reezlans) {
			if(b.reezlan==p) {
				return true;
			}
		}
		return false;
	}
	
	public void update() {
		for(int i = 0; i<reezlans.size(); i++) {
			if( reezlans.get(i).reezlan.getAir() == 300 || reezlans.get(i).reezlan.getAir() < 0 ) {
				toRemove.add(reezlans.get(i));
				continue;
			}
			reezlans.get(i).drown();
		}
		reezlans.removeAll(toRemove);
	}
}
