package com.zephaniahnoah.nexusraces;

import com.zephaniahnoah.nexusraces.items.ModItems;

import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.util.text.TextFormatting;

public enum PlayerClass {
	Human(TextFormatting.WHITE, ModItems.motifHuman, ModItems.flawlessDiamond), Elf(TextFormatting.YELLOW, ModItems.motifElf, ModItems.flawlessLapis), Dwarf(TextFormatting.BLUE, ModItems.motifDwarf, ModItems.flawlessIronNugget), Testificate(TextFormatting.GOLD, ModItems.motifTestificate, ModItems.flawlessEmerald),
	Kalot(TextFormatting.DARK_PURPLE, ModItems.motifKalot, ModItems.flawlessAmber), Tekkin(TextFormatting.DARK_GRAY, ModItems.motifTekkin, ModItems.flawlessRedstone), Awakened(TextFormatting.DARK_GREEN, ModItems.motifAwakened, ModItems.flawlessCarbonite), Gaian(TextFormatting.GREEN, ModItems.motifGaian, ModItems.flawlessSunstone),
	Lupithian(TextFormatting.GRAY, ModItems.motifLupithian, ModItems.flawlessMoonstone), HellBorne(TextFormatting.RED, ModItems.motifHellBorne, ModItems.flawlessNetherShard), Reezlan(TextFormatting.AQUA, ModItems.motifReezlan, ModItems.flawlessGoldNugget),
	Marrillian(TextFormatting.DARK_BLUE, ModItems.motifMarrillian, ModItems.flawlessPrismarine), None(TextFormatting.WHITE, Items.AIR, Items.DIAMOND);

	public TextFormatting color;
	public Item motifItem;
	public Item gemstoneItem;

	PlayerClass(TextFormatting d, Item m, Item g) {
		this.color = d;
		this.motifItem = m;
		this.gemstoneItem = g;
	}
}
