package com.zephaniahnoah.nexusraces.blocks;

import com.zephaniahnoah.nexusraces.Main;
import com.zephaniahnoah.nexusraces.items.ModItems;

import net.minecraft.block.BlockOre;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.math.BlockPos;

import java.util.Random;

public class ModBlockTekkiniteOre extends BlockOre {
    public  ModBlockTekkiniteOre(String name) {
        super();
        setRegistryName(name);
        setUnlocalizedName(Main.MODID + "." + name);
        ModBlocks.BLOCKS.add(this);
        ModBlocks.BLOCKITEMS.add(new ItemBlock(this).setRegistryName(this.getRegistryName()));
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return ModItems.roughTekkinite;
    }
    @Override
    public int quantityDropped(Random random) {
        return 1;
    }
    @Override
    public int getExpDrop(IBlockState state, net.minecraft.world.IBlockAccess world, BlockPos pos, int fortune) {
        return 0;
    }

}
