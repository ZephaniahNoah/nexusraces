package com.zephaniahnoah.nexusraces.blocks;

import com.zephaniahnoah.nexusraces.Main;
import com.zephaniahnoah.nexusraces.items.ModItems;

import net.minecraft.block.BlockOre;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

import java.util.Random;

public class ModBlockFireChalkOre extends BlockOre {
    public ModBlockFireChalkOre(String name) {
        super();
        setRegistryName(name);
        setUnlocalizedName(Main.MODID + "." + name);
        ModBlocks.BLOCKS.add(this);
        ModBlocks.BLOCKITEMS.add(new ItemBlock(this).setRegistryName(this.getRegistryName()));
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return ModItems.fireChalk;
    }
    @Override
    public int quantityDropped(Random random) {
        return 4 + random.nextInt(5);
    }
    @Override
    public int getExpDrop(IBlockState state, net.minecraft.world.IBlockAccess world, BlockPos pos, int fortune) {
        Random rand = world instanceof World ? ((World) world).rand : new Random();
        return MathHelper.getInt(rand, 1, 3);
    }
}
