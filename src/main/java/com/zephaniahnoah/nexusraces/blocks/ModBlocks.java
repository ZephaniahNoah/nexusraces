package com.zephaniahnoah.nexusraces.blocks;

import java.util.ArrayList;

import com.zephaniahnoah.nexusraces.Main;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@Mod.EventBusSubscriber(modid= Main.MODID)
public class ModBlocks {
    // TO ADD BLOCKS
/*  First, create a private static final Block Variable, using the ModBlock class
    Second, modify the block if necessary in init()
    Third, Register the ItemBlock in registerItemBlocks. Helper method is provided
    Fourth, make a blockname.json file in assets.lakraces.blockstates
    Fifth, make a blockname.json file in assets.lakraces.models.block
    Sixth, make a blockname.json file in assets.lakraces.models.item
    Seventh, add a line to the en_us.lang file in assets.lakraces.lang
 */
/*
    HARVEST LEVELS & GENERATION NOTES
    BOULDER - semi uncommon on the surface as a structure. and semi-rare as a vein underground. they'de either require stone or iron to mine up.
    CRYING OBSIDIAN - when you make obsidian via combining water over lava, there'd be a 1-10 chance for crying obsidian to spawn in place of a normal obsidian block
                    - if not possible, just have them spawn deep underground close to lava and water pits.
    FIRE CHALK ORE - require wood or stone to mine. they can spawn at any depth and are a bit more uncommon than iron. Drops fire chalk in the same way redstone or lapis do
    FORGED OBSIDIAN -  spawns naturally around the same areas as natural obsidian veins. so... near lava n such. they'de likely require something in between iron and diamond.
    GEMSTONE ORE - most likely require diamond. drops any 1 of the flawless gems depending on the race that mined it up. can be found at any depth but like diamond only spawns 1 per chunk and doesnt come in veins
                                                - you havnt chosen a race yet and you mine it up, then perhaps it'd just drop 1 random flawless gem, or 1 normal diamond
    MOTIF ORE - would likely require iron, and would be found at about diamond level, and likely about as rare. (doesnt come in veins either)
    - Motif Crystal ore and Flawless Gem Ore should not be affected by the fortune enchantment
    SALT ORE - would spawn at higher levels and the rarity should be in between coal and iron i think.... or just around iron rarity. it would require a wooden pick to mine and drops salt (in the same way as redstone or lapis)
    SOUL STONE ORE -  drops a blank Soul Stone when mined. requires Iron to mine and is about as rare as gold or lapis perhaps. around gold-emerald level
    TEKKINITE ORE -  requires iron to mine and is found in the deepest parts of the world. just slightly above bedrock level. Drops rough tekkinite - smelted into tekkinite
    VOID CRYSTAL ORE - they spawn at around diamond level. are in between diamond and gold rarity. 2 Drops, void crystals like lapis and redstone, and Void hearts 1 in 20 chance to drop a Void Heart when mined, each level of fortune however increases the odds by 1. (fortune 1 being 1-19 chance, fortune 2 being 1-18 chance,
 */
    public static final ArrayList<Block> BLOCKS = new ArrayList<Block>();
    public static final ArrayList<Item> BLOCKITEMS = new ArrayList<Item>();
    private static final Block cryingObsidian = new ModBlock("crying_obsidian", Material.ROCK).setHardness(50.0f);
    public static final Block crystalLeavesEmerald = new ModBlockLeavesEmerald("crystal_leaves_emerald");
    public static final Block crystalLeavesRuby = new ModBlockLeavesRuby("crystal_leaves_ruby");
    public static final Block crystalLeavesSapphire = new ModBlockLeavesSapphire("crystal_leaves_sapphire");
    public static final Block crystalLeavesVoid = new ModBlockLeavesVoid("crystal_leaves_void");
    public static final Block crystalLogEmerald = new ModBlockLogEmerald("crystal_log_emerald");
    public static final Block crystalLogRuby = new ModBlockLogRuby("crystal_log_ruby");
    public static final Block crystalLogSapphire = new ModBlockLogSapphire("crystal_log_sapphire");
    public static final Block crystalLogVoid = new ModBlockLogVoid("crystal_log_void");
    public static final Block fireChalkOre = new ModBlockFireChalkOre("fire_chalk_ore");
    private static final Block fireCokeBlock = new ModBlock("fire_coke_block", Material.IRON).setHardness(5.0F).setResistance(10.0F);
    private static final Block forgedObsidianOre = new ModBlock("forged_obsidian_ore", Material.ROCK).setHardness(3.0F).setResistance(5.0F);
    private static final Block goldBlock = new ModBlock("gold_block", Material.IRON).setHardness(5.0F).setResistance(10.0F);
    private static final Block saltBlock = new ModBlock("salt_block", Material.ROCK).setHardness(2.0F).setResistance(5.0F);
    public static final Block tekkiniteOre = new ModBlockTekkiniteOre("tekkinite_ore");
    private static final Block motifConstructor = new ModBlock("motif_constructor", Material.ANVIL).setHardness(5.0F).setResistance(2000.0F);
    private static final Block racialCraftingTable = new ModBlock("racial_crafting_table", Material.ROCK).setHardness(2.0F).setResistance(5.0F);
    private static final Block motifTuner = new ModBlock("motif_tuner", Material.ROCK).setHardness(2.0F).setResistance(5.0F);
    public static final Block motifCrystalOre = new ModBlockMotifOre("motif_crystal_ore");
    public static final Block netherMotifCrystalOre = new ModBlockMotifOre("motif_crystal_nether_ore");
    public static final Block endMotifCrystalOre = new ModBlockMotifOre("motif_crystal_end_ore");
    public static final Block voidCrystalOre = new ModBlockVoidCrystalOre("void_crystal_ore");
    public static final Block netherVoidCrystalOre = new ModBlockVoidCrystalOre("void_crystal_nether_ore");
    public static final Block endVoidCrystalOre = new ModBlockVoidCrystalOre("void_crystal_end_ore");
    private static final Block boulder = new ModBlock("boulder", Material.ROCK).setHardness(2.0F).setResistance(5.0F);
    public static final Block gemstoneOre = new ModBlockGemstoneOre("gemstone_ore");
    public static final Block netherGemstoneOre = new ModBlockGemstoneOre("gemstone_nether_ore");
    public static final Block endGemstoneOre = new ModBlockGemstoneOre("gemstone_end_ore");
    public static final Block soulStoneOre = new ModBlockSoulStoneOre("soul_stone_ore");
    public static final Block saltOre = new ModBlockSaltOre("salt_ore");
    public static final Block emeraldCrystalSapling = new ModBlockEmeraldSapling("crystal_sapling_emerald");
    public static final Block rubyCrystalSapling = new ModBlockEmeraldSapling("crystal_sapling_ruby");
    public static final Block sapphireCrystalSapling = new ModBlockEmeraldSapling("crystal_sapling_sapphire");
    public static final Block voidCrystalSapling = new ModBlockEmeraldSapling("crystal_sapling_void");

    public static void init() {
        cryingObsidian.setHarvestLevel("pickaxe", 3);
        crystalLogEmerald.setHarvestLevel("pickaxe", 0);
        crystalLogRuby.setHarvestLevel("pickaxe", 1);
        crystalLogSapphire.setHarvestLevel("pickaxe", 2);
        crystalLogVoid.setHarvestLevel("pickaxe", 3);
        fireChalkOre.setHarvestLevel("pickaxe", 1);
        forgedObsidianOre.setHarvestLevel("pickaxe", 3);
        gemstoneOre.setHarvestLevel("pickaxe", 3);
        endGemstoneOre.setHarvestLevel("pickaxe", 3);
        netherGemstoneOre.setHarvestLevel("pickaxe", 3);
        boulder.setHarvestLevel("pickaxe", 1);
        motifCrystalOre.setHarvestLevel("pickaxe", 2);
        endMotifCrystalOre.setHarvestLevel("pickaxe", 2);
        netherMotifCrystalOre.setHarvestLevel("pickaxe", 2);
        saltOre.setHarvestLevel("pickaxe", 0);
        tekkiniteOre.setHarvestLevel("pickaxe", 2);
        soulStoneOre.setHarvestLevel("pickaxe", 2);
        voidCrystalOre.setHarvestLevel("pickaxe", 3);
        endVoidCrystalOre.setHarvestLevel("pickaxe", 3);
        netherVoidCrystalOre.setHarvestLevel("pickaxe", 3);
    }
    @SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event) {
        for(Block block : BLOCKS) {
        	block.setCreativeTab(Main.tab);
            event.getRegistry().register(block);
        }
    }

    @SubscribeEvent
    public static void registerItemBlocks(RegistryEvent.Register<Item> event) {
    	for(Item item : BLOCKITEMS) {
            event.getRegistry().register(item);
        }
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public static void registerRenders(ModelRegistryEvent event) {
        for(Block block : BLOCKS) {
            registerRender(Item.getItemFromBlock(block));
        }
    }

    private static void registerRender(Item item) {
        ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation( item.getRegistryName(), "inventory"));
    }
}