package com.zephaniahnoah.nexusraces.blocks;

import com.zephaniahnoah.nexusraces.Main;
import com.zephaniahnoah.nexusraces.items.ModItems;

import net.minecraft.block.BlockOre;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import java.util.Random;

public class ModBlockVoidCrystalOre extends BlockOre {

    public ModBlockVoidCrystalOre(String name) {
        super();
        setRegistryName(name);
        setUnlocalizedName(Main.MODID + "." + name);
        ModBlocks.BLOCKS.add(this);
        ModBlocks.BLOCKITEMS.add(new ItemBlock(this).setRegistryName(this.getRegistryName()));
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return ModItems.voidCrystal;
    }

    @Override
    public void getDrops(NonNullList<ItemStack> drops, IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
        Random rand = world instanceof World ? ((World) world).rand : RANDOM;
        int numCrystals = super.quantityDroppedWithBonus(fortune, rand);
        drops.add(new ItemStack(this.getItemDropped(state, rand, fortune), numCrystals));
        if (rollVoidHeart(rand, fortune)) {
            drops.add(new ItemStack(ModItems.voidHeart, 1));
        }
    }

    private boolean rollVoidHeart(Random rand, int fortune) {
        // Clamp fortune
        if(fortune < 0) {
            fortune = 0;
        } else if(fortune >= 20) {
            fortune = 19;
        }
        float chance = 1.0f/(20-fortune);
        return rand.nextFloat() < chance;
    }
}