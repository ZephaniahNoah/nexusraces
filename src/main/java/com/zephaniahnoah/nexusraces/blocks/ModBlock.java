package com.zephaniahnoah.nexusraces.blocks;

import com.zephaniahnoah.nexusraces.Main;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemBlock;

public class ModBlock extends Block {
    public ModBlock(String name, Material material) {
        super(material);
        setRegistryName(name);
        setUnlocalizedName(Main.MODID + "." + name);
        ModBlocks.BLOCKS.add(this);
        ModBlocks.BLOCKITEMS.add(new ItemBlock(this).setRegistryName(this.getRegistryName()));
    }
}
