package com.zephaniahnoah.nexusraces.blocks;

import net.minecraft.block.BlockOre;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import javax.annotation.Nullable;

import com.zephaniahnoah.nexusraces.Main;
import com.zephaniahnoah.nexusraces.PlayerClass;
import com.zephaniahnoah.nexusraces.PlayerData;

import java.util.Random;

public class ModBlockMotifOre extends BlockOre {
    private PlayerClass blockBreakerClass = PlayerClass.None;

    public ModBlockMotifOre(String name) {
        super();
        setRegistryName(name);
        setUnlocalizedName(Main.MODID + "." + name);
        ModBlocks.BLOCKS.add(this);
        ModBlocks.BLOCKITEMS.add(new ItemBlock(this).setRegistryName(this.getRegistryName()));
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return blockBreakerClass.motifItem;
    }

    @Override
    public void harvestBlock(World worldIn, EntityPlayer player, BlockPos pos, IBlockState state, @Nullable TileEntity te, ItemStack stack){
    	blockBreakerClass = PlayerData.getPlayerClass(player);
        super.harvestBlock(worldIn, player, pos, state, te, stack);
    }
}
