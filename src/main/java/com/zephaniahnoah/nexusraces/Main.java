package com.zephaniahnoah.nexusraces;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import com.zephaniahnoah.nexusraces.blocks.ModBlocks;
import com.zephaniahnoah.nexusraces.commands.SetClass;
import com.zephaniahnoah.nexusraces.gui.GuiChooseClass;
import com.zephaniahnoah.nexusraces.items.ModItems;
import com.zephaniahnoah.nexusraces.packets.BookListPacketHandler;
import com.zephaniahnoah.nexusraces.packets.GetClassPacketHandler;
import com.zephaniahnoah.nexusraces.packets.PlayerSetClassPacketHandler;
import com.zephaniahnoah.nexusraces.packets.PlayerSpeedPacketHandler;
import com.zephaniahnoah.nexusraces.packets.SendBookList;
import com.zephaniahnoah.nexusraces.packets.SendGetClass;
import com.zephaniahnoah.nexusraces.packets.SendPlayerSpeed;
import com.zephaniahnoah.nexusraces.packets.SendSetPlayerClass;
import com.zephaniahnoah.nexusraces.packets.SendSpellBook;
import com.zephaniahnoah.nexusraces.packets.SpellBookPacketHandler;

import net.minecraft.block.Block;
import net.minecraft.block.BlockOre;
import net.minecraft.client.Minecraft;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentUntouching;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EnumCreatureAttribute;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.boss.EntityDragon;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityEvoker;
import net.minecraft.entity.monster.EntityGhast;
import net.minecraft.entity.monster.EntityMagmaCube;
import net.minecraft.entity.monster.EntityVex;
import net.minecraft.entity.monster.EntityVindicator;
import net.minecraft.entity.monster.EntityWitherSkeleton;
import net.minecraft.entity.passive.EntityWolf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.projectile.EntityPotion;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.storage.loot.LootContext;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.enchanting.EnchantmentLevelSetEvent;
import net.minecraftforge.event.entity.EntityEvent.EntityConstructing;
import net.minecraftforge.event.entity.ThrowableImpactEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingJumpEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.living.LivingSetAttackTargetEvent;
import net.minecraftforge.event.entity.living.PotionEvent.PotionApplicableEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.NameFormat;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.EntityInteractSpecific;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.ItemCraftedEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerRespawnEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

// TODO Gaians have night vision in sun?
// TODO Kalots eating poisonous food causes crash. no food stats on server side?

@SuppressWarnings("deprecation")
@Mod.EventBusSubscriber
@Mod(modid = Main.MODID, name = Main.NAME, version = Main.VERSION)
public class Main {

	public static final String MODID = "nexusraces";
	public static final String NAME = "Races of the Nexus";
	public static final String VERSION = "1.8";
	private Random rand = new Random();
	private boolean removeHungerEffect = false;
	private List<EntityPlayer> affectedByHunger = new ArrayList<EntityPlayer>();
	//private List<EntityPlayer> reezlans = new ArrayList<EntityPlayer>();
	private List<AwakenedAttack> attacking = new ArrayList<AwakenedAttack>();
	private int timer = 0;
	private boolean check = false;
	private int tick = 0;
	private ReezlanBreathCounter breathCounter;
	//private int gamma;
	private boolean isBright = false;
	private boolean lastCondition = false;
	List<Block> clearBlocks = Arrays.asList(Blocks.AIR, Blocks.LEAVES, Blocks.LEAVES2, Blocks.TALLGRASS, Blocks.DOUBLE_PLANT);
	private int kalotTimer = 0;
	private List<List<EntityPlayer>> kalots;
	
	public static final CreativeTabs tab = (new CreativeTabs(MODID) {
		@Override
		public ItemStack getTabIconItem() {
			return new ItemStack(ModItems.classItem);
		}
	});

	public static final SimpleNetworkWrapper INSTANCE = NetworkRegistry.INSTANCE.newSimpleChannel(MODID);

	@EventHandler
	public void preInit(FMLPreInitializationEvent e) {
		PlayerData.init(new File(e.getModConfigurationDirectory().getAbsolutePath() + File.separator + MODID + "PlayerData.cfg"));
		Config.init(new File(e.getModConfigurationDirectory().getAbsolutePath() + File.separator + MODID + ".cfg"));
		
		MinecraftForge.EVENT_BUS.register(this);
		MinecraftForge.EVENT_BUS.register(ModItems.class);
		MinecraftForge.EVENT_BUS.register(ModBlocks.class);
		ModBlocks.init();
	}

	@EventHandler
	public void init(FMLInitializationEvent e) {
		INSTANCE.registerMessage(PlayerSetClassPacketHandler.class, SendSetPlayerClass.class, 0, Side.SERVER);
		INSTANCE.registerMessage(SpellBookPacketHandler.class,   SendSpellBook.class,   1, Side.SERVER);
		INSTANCE.registerMessage(PlayerSpeedPacketHandler.class, SendPlayerSpeed.class, 2, Side.SERVER);
		INSTANCE.registerMessage(BookListPacketHandler.class, SendBookList.class, 3, Side.CLIENT);
		INSTANCE.registerMessage(GetClassPacketHandler.class, SendGetClass.class, 4, Side.CLIENT);
		GameRegistry.registerWorldGenerator(new ModOreGeneration(), 3);
		breathCounter = new ReezlanBreathCounter();
	}

	@EventHandler
	public void initServer(FMLServerStartingEvent e) {
		e.registerServerCommand(new SetClass());
	}

	public static void selectClass(EntityPlayer p){
		if(p.getEntityWorld().isRemote) {
			GuiChooseClass.Instantiate(p);
		}
	}

	@SubscribeEvent
	public void onMakePlayer(EntityConstructing e) {
		if(e.getEntity() instanceof EntityPlayer) {
			//((EntityPlayer)e.getEntity()).getAttributeMap().registerAttribute(speedAttribute);
		}
	}

	@SubscribeEvent
	public void onClickEntity(EntityInteractSpecific e) {
		if(PlayerData.getPlayerClass(e.getEntityPlayer())==PlayerClass.Lupithian) {
			if(e.getEntityPlayer().getHeldItemMainhand().getItem() == net.minecraft.init.Items.BONE) {
				if(e.getTarget() instanceof EntityWolf) {
					((EntityWolf)e.getTarget()).setTamedBy(e.getEntityPlayer());
				}
			}
		}
	}

	@SubscribeEvent
	@SideOnly(Side.SERVER)
	public void onLogIn(PlayerLoggedInEvent e) {
		INSTANCE.sendTo(new SendBookList(BookListPacketHandler.booksStart), (EntityPlayerMP) e.player);
		for(ItemStack i:Config.books) {
			INSTANCE.sendTo(new SendBookList(i.getMetadata()), (EntityPlayerMP) e.player);
		}
		INSTANCE.sendTo(new SendGetClass(PlayerData.getPlayerClass(e.player)),(EntityPlayerMP) e.player);
		try{
			updateEffects(e.player);
			//resetEffects(e.player);
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		if(!e.player.inventory.hasItemStack(new ItemStack(ModItems.classItem))) {
			if(PlayerData.getPlayerClass(e.player) == PlayerClass.None) {
				e.player.addItemStackToInventory(new ItemStack(ModItems.classItem));
			}
		}
	}
	
	@SubscribeEvent
	public void onNameFormat(NameFormat e) {
		EntityPlayer p = e.getEntityPlayer();
		PlayerClass c = PlayerData.getPlayerClass(p);
		if(c!=PlayerClass.None) {
			e.setDisplayname(c.color + p.getName() + " the " + PlayerData.getPlayerClass(p));
		}
	}

	public static void resetEffects(EntityPlayer p) {
		p.refreshDisplayName();
		try{
			if(p.world.isRemote) {
				INSTANCE.sendToServer(new SendPlayerSpeed(0d));
			} else {
				PlayerSpeedPacketHandler.setPlayerSpeed(p, 0d);
			}
			p.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(20.0);
			p.getAttributeMap().getAttributeInstance(SharedMonsterAttributes.LUCK).setBaseValue(0);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	//Probably not needed
	public static void updateEffects(EntityPlayer p) {
		switch(PlayerData.getPlayerClass(p)){
			case Dwarf:
				PlayerSpeedPacketHandler.setPlayerSpeed(p, -.015d);
				break;
			case Testificate:
				// Unused
				break;
			default:
				break;
		}
	}

	@SubscribeEvent
	public void onJump(LivingJumpEvent e) {
		if (e.getEntity() instanceof EntityPlayer && PlayerData.getPlayerClass((EntityPlayer)e.getEntity()) == PlayerClass.Kalot)
	    {
			e.getEntity().motionY += 0.14D;
			e.getEntity().fallDistance=-2;
	    }
	}

	@SubscribeEvent
	public static void onDig(PlayerEvent.BreakSpeed e) {
		EntityPlayer p = e.getEntityPlayer();
		if(PlayerData.getPlayerClass(e.getEntityPlayer()) == PlayerClass.Dwarf) {
			if(p.getMaxHealth() == p.getHealth() && !p.getFoodStats().needFood()) {
				e.setNewSpeed(e.getOriginalSpeed() + 6);
			}
		} else if(PlayerData.getPlayerClass(e.getEntityPlayer()) == PlayerClass.Reezlan && e.getEntityPlayer().isWet()) {
			e.setNewSpeed(e.getOriginalSpeed() + 10);
		}
	}

	@SubscribeEvent
	public void onEnchant(EnchantmentLevelSetEvent e) {
		//if(PlayerData.getPlayerClass(e.getPlayer()) == PlayerClass.Elf) {
		//	e.setLevel(e.getLevel()+8);
		//}
	}

	@SubscribeEvent
	public void onDeath(LivingDeathEvent e) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		if(e.getEntity() instanceof EntityPlayer) {
			EntityPlayer p = (EntityPlayer)e.getEntity();
			AwakenedAttack remove = null;
			for(AwakenedAttack a : attacking) {
				if(a.p == p) {
					remove = a;
				}
			}
			if(remove!=null) {
				attacking.remove(remove);
			}
		}
		if(e.getSource().getTrueSource() instanceof EntityPlayer) {
			EntityPlayer killer = (EntityPlayer)e.getSource().getTrueSource();
			if(PlayerData.getPlayerClass(killer) == PlayerClass.Kalot) {
				if(e.getEntityLiving().getCreatureAttribute() == EnumCreatureAttribute.ARTHROPOD ) {
					Entity arthropod = e.getEntity();
					World w = arthropod.world;
					try {
						Method m = arthropod.getClass().getDeclaredMethod("getLootTable");
						m.setAccessible(true);
						ResourceLocation loc = (ResourceLocation) m.invoke(arthropod);
						LootContext.Builder lootcontext$builder = new LootContext.Builder((WorldServer)w).withLootedEntity(arthropod).withPlayer(killer);//.withLuck(killer.getLuck());
						
						int lootingModifier = EnchantmentHelper.getEnchantmentLevel(Enchantment.getEnchantmentByID(21), killer.getHeldItemMainhand());
						
						for(ItemStack item : w.getLootTableManager().getLootTableFromLocation(loc).generateLootForPools(rand, lootcontext$builder.build())) {

					        if (item != null)
					        {
					            int i = this.rand.nextInt(3);

					            if (lootingModifier > 0)
					            {
					                i += this.rand.nextInt(lootingModifier + 1);
					            }

					            for (int j = 0; j < i; ++j)
					            {
					            	w.spawnEntity(new EntityItem(w, arthropod.posX, arthropod.posY, arthropod.posZ, item));
					            }
					        }
						}
					} catch (NoSuchMethodException e1) {
						e1.printStackTrace();
					} catch (SecurityException e1) {
						e1.printStackTrace();
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onEffect(PotionApplicableEvent e) {
		if(e.getEntity() instanceof EntityPlayer) {
			EntityPlayer p = (EntityPlayer)e.getEntity();
			if(e.getPotionEffect().getEffectName()=="effect.hunger") {
				if(PlayerData.getPlayerClass(p) == PlayerClass.Kalot || PlayerData.getPlayerClass(p) == PlayerClass.Awakened || PlayerData.getPlayerClass(p) == PlayerClass.HellBorne) {
					removeHungerEffect = true;
					affectedByHunger.add(p);
				}
			}
		}
	}

	@SubscribeEvent
	public void onEat(LivingEntityUseItemEvent.Finish e) {
		if(e.getEntity() instanceof EntityPlayer) {
			EntityPlayer p = (EntityPlayer)e.getEntity();
			if(PlayerData.getPlayerClass(p) == PlayerClass.Kalot) {
				if(e.getResultStack().getItem() instanceof ItemFood) {
					ItemFood food = (ItemFood)e.getResultStack().getItem();
					if(food.getHealAmount(null) <3) {
						p.getFoodStats().setFoodSaturationLevel(p.getFoodStats().getSaturationLevel() + 2);
					}
				}
			}
		}
	}

	@SubscribeEvent(priority=EventPriority.NORMAL, receiveCanceled=true)
	@SideOnly(Side.SERVER)
	public void one(TickEvent.PlayerTickEvent e) {
		if(removeHungerEffect) {
			for(EntityPlayer pl : affectedByHunger) {
				pl.removePotionEffect(Potion.getPotionById(17));
			}
			removeHungerEffect = false;
		}
		if(check) {
			List<AwakenedAttack> remove = new ArrayList<AwakenedAttack>();
			for(AwakenedAttack a : attacking) {
				if(a.update()) {
					remove.add(a);
				}
			}
			attacking.removeAll(remove);
			EntityPlayer p = e.player;
			switch(PlayerData.getPlayerClass(p)) {
				case Reezlan:
					if(p.getAir()<300&&p.getAir()>0) {
						if(!breathCounter.has(p)) {
							breathCounter.add(p);
						}
					}
					if(p.isInWater()) {
						p.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(24.0);
						PlayerSpeedPacketHandler.setPlayerSpeed(p, 0d);
					} else {
						p.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(20.0);
						PlayerSpeedPacketHandler.setPlayerSpeed(p, -.015d);
					}
					break;
				case HellBorne:
					p.setFire(0);
					if(p.isInWater()) {
						if(p.getAir()-14>0) {
							p.setAir(p.getAir()-14);
						}
					}
					break;
				case Awakened:
					if(!p.isWet() && inSunlight(p)) {
						ItemStack i = p.inventory.armorInventory.get(3);
						if(i.getItem() == net.minecraft.init.Items.AIR) {
							p.setFire(4);
						} else {
							if(i.getItem() instanceof ItemArmor) {
								if(rand.nextInt(10) == 3) {
									i.damageItem(1, p);
								}
							}
						}
					}
					break;
				case Gaian:
					if(inSunlightIgnoreClear(p)) {
						p.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(28.0);
						PlayerSpeedPacketHandler.setPlayerSpeed(p, -.015d);
					} else if(inMoonlight(p)){
						p.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(20.0);
						PlayerSpeedPacketHandler.setPlayerSpeed(p, .05d);
					} else {
						p.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(20.0);
						PlayerSpeedPacketHandler.setPlayerSpeed(p, 0d);
					}
					break;
				case Kalot:
					if(inSunlight(p)) {
						PlayerSpeedPacketHandler.setPlayerSpeed(p, 0.05d);
					} else if(inMoonlight(p)){
						PlayerSpeedPacketHandler.setPlayerSpeed(p, -.02d);
					} else {
						PlayerSpeedPacketHandler.setPlayerSpeed(p, 0d);
					}
					break;
				case Tekkin:
					if(p.isWet() || inRain(p)) {
						PlayerSpeedPacketHandler.setPlayerSpeed(p, -.02d);
					} else {
						PlayerSpeedPacketHandler.setPlayerSpeed(p, 0d);
					}
					break;
				case Lupithian:
					if(inMoonlight(p)) {
						if(fullMoon(p.world)) {
							PlayerSpeedPacketHandler.setPlayerSpeed(p, .1d);
						} else {
							PlayerSpeedPacketHandler.setPlayerSpeed(p, .05d);
						}
					} else if(inSunlightIgnoreClear(p)) {
						PlayerSpeedPacketHandler.setPlayerSpeed(p, -.02d);
					} else {
						PlayerSpeedPacketHandler.setPlayerSpeed(p, 0d);
					}
					break;
				case Testificate:
					if(p.getHealth() == p.getMaxHealth()) {
						p.getAttributeMap().getAttributeInstance(SharedMonsterAttributes.LUCK).setBaseValue(1);
					} else {
						p.getAttributeMap().getAttributeInstance(SharedMonsterAttributes.LUCK).setBaseValue(0);
					}
					break;
				default:
					break;
			}
		}
	}

	@SubscribeEvent(priority=EventPriority.NORMAL, receiveCanceled=true)
	@SideOnly(Side.SERVER)
	public void onTickEvent(TickEvent.ServerTickEvent e) {
		check = false;
		kalots = new ArrayList<List<EntityPlayer>>();
		if(kalotTimer == 60) {//TODO TEST
			List<EntityPlayerMP> players = FMLCommonHandler.instance().getMinecraftServerInstance().getServer().getPlayerList().getPlayers();
			for(int i = 0; i < players.size(); i++) {
				EntityPlayer p = players.get(i);
				if(PlayerData.getPlayerClass(p) == PlayerClass.Kalot) {
					kalots.add(new ArrayList<EntityPlayer>());
					kalots.get(kalots.size()-1).add(p);
					for(EntityPlayer pa : players) {
						if(PlayerData.getPlayerClass(pa) == PlayerClass.Kalot) {
							//If pa is within 10 blocks of p
							if(p.getDistance(p) < 10) {
								kalots.get(kalots.size()-1).add(pa);
							}
						}
					}
				}
			}
			kalotTimer = 0;
		}
		if(tick==20) {
			breathCounter.update();
			check =true;
			tick = 0;
		}
		kalotTimer++;
		tick++;
	}

	@SubscribeEvent
	public void onTarget(LivingSetAttackTargetEvent e) {
		if(e.getTarget() instanceof EntityPlayer) {// && e.getEntityLiving().getCreatureAttribute() == EnumCreatureAttribute.UNDEAD) {
			EntityPlayer p = (EntityPlayer)e.getTarget();
			Entity n = e.getEntityLiving();
			if(PlayerData.getPlayerClass(p) == PlayerClass.Awakened) {
				// try (n instanceof EntityLiving && !hostiles.contains(n.getClass()))
				if(n instanceof EntityLiving && !(n instanceof EntityEvoker) && !(n instanceof EntityVex) && !(n instanceof EntityVindicator) && !(n instanceof EntityDragon) && !(n instanceof EntityWither) && !(n instanceof EntityGhast) && !(n instanceof EntityMagmaCube) && !(n instanceof EntityWitherSkeleton) && n.isNonBoss()) {
					boolean found = false;
					for(AwakenedAttack a : attacking) {
						if(a.p == p) {
							found = true;
						}
					}
					if(!found) {
						if(e.getEntityLiving().getAttackingEntity() != p) {
							((EntityLiving) e.getEntityLiving()).setAttackTarget(null);
						} else {
							attacking.add(new AwakenedAttack(p, 10));
						}
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onImpact(ThrowableImpactEvent e) {
		if(e.getRayTraceResult().entityHit instanceof EntityPlayer && e.getEntityThrowable() instanceof EntityPotion) {
			EntityPotion pot = (EntityPotion) e.getEntityThrowable();
			EntityPlayer p = (EntityPlayer) e.getRayTraceResult().entityHit;
			if(pot.getPotion().getDisplayName().contains("Splash Potion of Healing") && PlayerData.getPlayerClass(p) == PlayerClass.Awakened) {
				e.setCanceled(true);
				p.setHealth(p.getHealth() - 6);
			}
		}
	}

	@SubscribeEvent(priority=EventPriority.NORMAL, receiveCanceled=true)
	@SideOnly(Side.CLIENT)
	public void onTickEvent(TickEvent.ClientTickEvent e) {
		if(Minecraft.getMinecraft()==null||Minecraft.getMinecraft().player==null) return;

		EntityPlayer p = Minecraft.getMinecraft().player;
																										// inMoonlight(p)
		if(PlayerData.getPlayerClass(p) == PlayerClass.Awakened || (PlayerData.getPlayerClass(p) == PlayerClass.Gaian && p.posY > 55 && !p.world.isDaytime()) || (PlayerData.getPlayerClass(p) == PlayerClass.Reezlan && p.getAir()<300)) {
			isBright = true;
			lastCondition = !isBright;
		} else {
			isBright = false;
		}
		if(lastCondition != isBright) {
			if(isBright) {
				Minecraft.getMinecraft().gameSettings.gammaSetting = 5f;
			} else {
				Minecraft.getMinecraft().gameSettings.gammaSetting = 0f;
			}
			lastCondition = isBright;
		}
		if(timer==20) {
			timer=0;
			//TODO is this needed?
			Minecraft.getMinecraft().world.calculateInitialSkylight();
		}
		timer++;
	}

	public boolean inMoonlight(EntityPlayer p) {
		return !p.world.provider.isDaytime() && !p.world.isRaining() && aboveGroundIgnoreClear(p);
	}
	
	public boolean aboveGroundIgnoreClear(EntityPlayer p) {
		int y = p.getEntityWorld().getHeight(p.getPosition()).getY();
		
		while(clearBlocks.contains(p.getEntityWorld().getBlockState(new BlockPos(p.posX, y, p.posZ)).getBlock())) {
			y--;
			if(y <= 0) {
				return true;
			}
		}
		return (p.getPosition().getY()+1) > y;
	}

	public boolean inSunlight(EntityPlayer p) {
		return p.world.provider.isDaytime() && !p.world.isRaining() && aboveGround(p);
	}
	
	public boolean inSunlightIgnoreClear(EntityPlayer p) {
		return p.world.provider.isDaytime() && !p.world.isRaining() && aboveGroundIgnoreClear(p);
	}

	public boolean fullMoon(World w) {
		return w.provider.getMoonPhase(w.getWorldTime())==0;
	}

	public boolean aboveGround(EntityPlayer p) {
		return p.posY+2 > p.getEntityWorld().getHeight(p.getPosition()).getY();
	}
	
	private boolean inRain(EntityPlayer p) {
		return aboveGround(p) && p.world.isRaining() && !(p.world.getBiome(new BlockPos(p.posX, p.posY, p.posZ)).getTempCategory() == Biome.TempCategory.WARM);
	}

	private boolean isFire(DamageSource s) {
		return s == DamageSource.IN_FIRE || s == DamageSource.ON_FIRE || s == DamageSource.LAVA || s == DamageSource.HOT_FLOOR;
	}

	@SubscribeEvent
	public void onAttack(LivingAttackEvent e) {
		if(e.getEntity() instanceof EntityPlayer) {
			EntityPlayer p = (EntityPlayer)e.getEntity();
			switch(PlayerData.getPlayerClass(p)) {
				case Awakened:
					if(e.getSource().isMagicDamage()) {
						e.setCanceled(true);
						p.heal(e.getAmount());
					}
					break;
				case HellBorne:
					if(isFire(e.getSource())) {
						e.setCanceled(true);
					}
					break;
				default:
					break;
			}
		}
	}

	@SubscribeEvent
	public void onHurt(LivingHurtEvent e) {
		if(e.getEntity() instanceof EntityPlayer) {
			EntityPlayer p = (EntityPlayer)e.getEntity();
			switch(PlayerData.getPlayerClass((EntityPlayer)e.getEntity())) {
				case Elf:
					if(!e.getSource().isProjectile() && e.getSource() == DamageSource.GENERIC) {
						e.setAmount((float) (e.getAmount() + e.getAmount()*.2));
					}
					break;
				case Testificate:
					e.setAmount((float) (e.getAmount() + e.getAmount()*.4));
					break;
				case Kalot:
					if(e.getSource() == DamageSource.FALL) {
						if(e.getAmount() < 1.3) {
							e.setCanceled(true);
							//p.fallDistance=0;
						} else {
							e.setAmount(e.getAmount() - e.getAmount() / 3);
						}
					} else if(isFire(e.getSource())) {
						e.setAmount(e.getAmount() + e.getAmount()/3);
					} else if(e.getSource().isMagicDamage()) {
						for(PotionEffect effect : p.getActivePotionEffects()) {
							if(effect.getEffectName()=="effect.poison") {
								if(e.getAmount()<=1.1) {
									e.setAmount(3);
								}
								break;
							}
						}
					}
					break;
				/*case Awakened:
					if(e.getSource().isMagicDamage()) {//TODO DELETE
						e.setCanceled(true);
						p.heal(e.getAmount());
						e.setAmount(0);
					}
					break;*/
				case Gaian:
					if(e.getSource()!=null) {
						if(isFire(e.getSource())) {
							e.setAmount(e.getAmount()+e.getAmount()/2);
						} else if(e.getSource().isMagicDamage()) {
							for(PotionEffect effect : p.getActivePotionEffects()) {
								if(effect.getEffectName()=="effect.poison") {
									if(e.getAmount()<=1.1) {
										e.setAmount(3);
									}
									break;
								}
							}
						}
						if(inSunlight(p)) {
							e.setAmount(e.getAmount() - e.getAmount()/4);
						} else {
							if(e.getSource() == DamageSource.GENERIC && aboveGround(p)) {
								e.setAmount(e.getAmount() + e.getAmount()/3);
							}
						}
					}
					break;
				case Lupithian:
					if(isFire(e.getSource())) {
						e.setAmount(e.getAmount()+e.getAmount()/2);
					}
					break;
				case HellBorne:
					if(p.dimension == -1) {
						e.setAmount(e.getAmount() - e.getAmount()/2);
					}
					break;
				default:
					break;
			}
		}

		if(e.getSource() instanceof EntityDamageSource) {
			Entity trueSource = ((EntityDamageSource)e.getSource()).getTrueSource();
			if(trueSource instanceof EntityPlayer) {
				EntityPlayer attacker = (EntityPlayer)((EntityDamageSource)e.getSource()).getTrueSource();
				
				switch(PlayerData.getPlayerClass(attacker)) {
					case Elf:
						if(e.getSource().isProjectile()) {
							e.setAmount((float) (e.getAmount() + e.getAmount()*.2));
						}
						break;
					case Tekkin:
						if((attacker.isWet() || (aboveGround(attacker) && attacker.world.isRaining()))) {
							e.setAmount((float) (e.getAmount() - e.getAmount()/4));
						}
						break;
					case Gaian:
						if(inMoonlight(attacker)) {
							e.setAmount(e.getAmount() + e.getAmount()/3);
						}
						break;
					case Lupithian:
						if(inMoonlight(attacker)) {
							if(fullMoon(attacker.world)) {
								e.setAmount(e.getAmount() + e.getAmount()/2);
							} else {
								e.setAmount(e.getAmount() + e.getAmount()/3);
							}
						} else if(inSunlight(attacker)) {
							e.setAmount(e.getAmount() - e.getAmount()/3);
						}
						break;
					case HellBorne:
						if(attacker.dimension == -1) {
							e.setAmount(e.getAmount() + e.getAmount()/3);
						}
						if(inSunlight(attacker)) {
							e.setAmount(e.getAmount() - e.getAmount()/3);
						}
						break;
					case Kalot:
						for(List<EntityPlayer> list : kalots) {//TODO TEST
							if(list.contains(attacker)) {
								int strength = 0;
								if(list.size()>2) {
									if(list.size()>3) {
										strength++;
										if(list.size()>4) {
											strength++;
										}
									}
									e.setAmount(e.getAmount() + 3 * strength);
									break;
								}
							}
						}
						break;
					default:
						break;
				}
			}

			if(e.getEntity() instanceof EntityPlayer) {
				if(PlayerData.getPlayerClass((EntityPlayer)e.getEntity()) == PlayerClass.Awakened) {
					e.setAmount(e.getAmount() + EnchantmentHelper.getEnchantmentLevel(Enchantment.getEnchantmentByID(17), trueSource.getHeldEquipment().iterator().next()) * 2.5F);
				}
				
				if(PlayerData.getPlayerClass((EntityPlayer)e.getEntity()) == PlayerClass.Kalot) {
					
					e.setAmount(e.getAmount() + EnchantmentHelper.getEnchantmentLevel(Enchantment.getEnchantmentByID(18), trueSource.getHeldEquipment().iterator().next()) * 2.5F);
					
					/*NBTTagList nbttaglist = trueSource.getHeldEquipment().iterator().next().getEnchantmentTagList();

		            for (int i = 0; i < nbttaglist.tagCount(); ++i)
		            {
		                int j = nbttaglist.getCompoundTagAt(i).getShort("id");
		                int k = nbttaglist.getCompoundTagAt(i).getShort("lvl");

		                if (j == 18)
		                {
		                	e.setAmount(e.getAmount() + k * 2.5F);
		                	break;
		                }
		            }*/
				}
			}
		}
	}

	@SubscribeEvent
	@SideOnly(Side.SERVER)
	public void onRespawn(PlayerRespawnEvent e) {
		updateEffects(e.player);
	}

	@SubscribeEvent
	public void onCraftItem(ItemCraftedEvent e) {// Doesn't work with shift click
		if(PlayerData.getPlayerClass(e.player)==PlayerClass.Tekkin) {
			if(!(e.crafting.getItem() == Item.getItemFromBlock(Blocks.REDSTONE_BLOCK))) {//== .get(0).get.contains(Ingredient.fromItems(net.minecraft.init.Items.REDSTONE))) {
				boolean found = false;
				if(CraftingManager.getRecipe(e.crafting.getItem().getRegistryName())==null) return;
				for(Ingredient i : CraftingManager.getRecipe(e.crafting.getItem().getRegistryName()).getIngredients()) {
					if(found)break;
					for(ItemStack stack : i.getMatchingStacks()) {
						if(stack.getItem() == net.minecraft.init.Items.REDSTONE) {
							found = true;
							e.player.addItemStackToInventory(new ItemStack(e.crafting.getItem()));
							break;
						}
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onBreak(BlockEvent.BreakEvent e) {
		EntityPlayer p = e.getPlayer();
		BlockPos pos = e.getPos();
		World w = e.getWorld();
		Block block = e.getWorld().getBlockState(pos).getBlock();
		if(!EnchantmentHelper.getEnchantments(p.getHeldItemMainhand()).containsKey((EnchantmentUntouching) Enchantment.REGISTRY.getObjectById(33))) {
			switch(PlayerData.getPlayerClass(p)){
			case Dwarf:
				if(block instanceof BlockOre) {
					if(block.getItemDropped(null, null, 0)!=Item.getItemFromBlock(block)) {
						w.spawnEntity(new EntityItem(w, pos.getX(), pos.getY(), pos.getZ(), new ItemStack(block.getItemDropped(null, null, 0), new Random().nextBoolean() ? 1 : 0, block == Blocks.LAPIS_ORE ? 4 : 0)));
					}
				}
				break;
			case Elf:
				if(block == Blocks.LAPIS_ORE) {
					w.spawnEntity(new EntityItem(w, pos.getX(), pos.getY(), pos.getZ(), new ItemStack(net.minecraft.init.Items.DYE, 3, 4)));
				}
				break;
			case Tekkin:
				int fortune = EnchantmentHelper.getEnchantmentLevel(Enchantment.getEnchantmentByID(35), p.getHeldItemMainhand());
				if(block == Blocks.REDSTONE_ORE) {
					w.spawnEntity(new EntityItem(w, pos.getX(), pos.getY(), pos.getZ(), new ItemStack(net.minecraft.init.Items.REDSTONE, 1 + fortune)));
				}
				if(block == Blocks.QUARTZ_ORE) {
					w.spawnEntity(new EntityItem(w, pos.getX(), pos.getY(), pos.getZ(), new ItemStack(net.minecraft.init.Items.QUARTZ, 1 + fortune)));
				}
				break;
			case HellBorne:
				if(p.dimension == -1) {
					if(block instanceof BlockOre) {
						for(ItemStack i : block.getDrops(e.getWorld(), pos, Block.getStateById(Block.getIdFromBlock(block)), 1)) {
							w.spawnEntity(new EntityItem(w, pos.getX(), pos.getY(), pos.getZ(), i));
						}
					} else if(block == Blocks.GLOWSTONE){
						e.setCanceled(true);
						block.breakBlock(e.getWorld(), pos, Block.getStateById(Block.getIdFromBlock(block)));
						w.spawnEntity(new EntityItem(w, pos.getX(), pos.getY(), pos.getZ(), new ItemStack(net.minecraft.init.Items.GLOWSTONE_DUST, 4)));
					}
				}
				break;
			case Testificate:
				if(block == Blocks.EMERALD_ORE) {
					w.spawnEntity(new EntityItem(w, pos.getX(), pos.getY(), pos.getZ(), new ItemStack(net.minecraft.init.Items.EMERALD)));
				}
				break;
			default:
				break;
			}
		}
	}
}































