package com.zephaniahnoah.nexusraces;

import net.minecraft.entity.player.EntityPlayer;

public class AwakenedAttack {
	
	public final EntityPlayer p;
	public int time;
	
	public AwakenedAttack(EntityPlayer p, int time) {
		this.p = p;
		this.time = time;
	}
	
	public boolean update() {
		this.time--;
		return time < 1;
	}
}
