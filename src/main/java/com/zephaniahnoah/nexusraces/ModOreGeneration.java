package com.zephaniahnoah.nexusraces;

import java.util.Random;

import com.google.common.base.Predicate;
import com.zephaniahnoah.nexusraces.blocks.ModBlocks;

import net.minecraft.block.state.IBlockState;
import net.minecraft.block.state.pattern.BlockMatcher;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraftforge.fml.common.IWorldGenerator;

public class ModOreGeneration implements IWorldGenerator {
    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        switch(world.provider.getDimension()) {
            // Overworld
            case 0:
                runGeneration(ModBlocks.fireChalkOre.getDefaultState(), 6, 10, 18, 40, BlockMatcher.forBlock(Blocks.STONE), world, random, chunkX, chunkZ);
                runGeneration(ModBlocks.soulStoneOre.getDefaultState(), 6, 6, 10, 30, BlockMatcher.forBlock(Blocks.STONE), world, random, chunkX, chunkZ);
                runGeneration(ModBlocks.saltOre.getDefaultState(), 10, 12, 20, 60, BlockMatcher.forBlock(Blocks.STONE), world, random, chunkX, chunkX);
                runGeneration(ModBlocks.gemstoneOre.getDefaultState(), 1, 1, 10, 50, BlockMatcher.forBlock(Blocks.STONE), world, random, chunkX, chunkZ);
                runGeneration(ModBlocks.voidCrystalOre.getDefaultState(), 6, 3, 6, 20, BlockMatcher.forBlock(Blocks.STONE), world, random, chunkX, chunkZ);
                runGeneration(ModBlocks.tekkiniteOre.getDefaultState(), 4, 5, 4, 16, BlockMatcher.forBlock(Blocks.STONE), world, random, chunkX, chunkZ);
                runGeneration(ModBlocks.motifCrystalOre.getDefaultState(), 1, 6, 8, 20, BlockMatcher.forBlock(Blocks.STONE), world, random, chunkX, chunkZ);
                break;
            // Nether
            case -1:
                runGeneration(ModBlocks.netherGemstoneOre.getDefaultState(), 1, 1, 20, 80, BlockMatcher.forBlock(Blocks.NETHERRACK), world, random, chunkX, chunkZ);
                runGeneration(ModBlocks.netherVoidCrystalOre.getDefaultState(), 6, 3, 20, 80, BlockMatcher.forBlock(Blocks.NETHERRACK), world, random, chunkX, chunkZ);
                runGeneration(ModBlocks.netherMotifCrystalOre.getDefaultState(), 1, 6, 20, 80, BlockMatcher.forBlock(Blocks.NETHERRACK), world, random, chunkX, chunkZ);
                break;
            // End
            case 1:
                runGeneration(ModBlocks.endGemstoneOre.getDefaultState(), 1, 1, 10, 60, BlockMatcher.forBlock(Blocks.END_STONE), world, random, chunkX, chunkZ);
                runGeneration(ModBlocks.endVoidCrystalOre.getDefaultState(), 6, 3, 10, 60, BlockMatcher.forBlock(Blocks.END_STONE), world, random, chunkX, chunkZ);
                runGeneration(ModBlocks.endMotifCrystalOre.getDefaultState(), 1, 6, 10, 60, BlockMatcher.forBlock(Blocks.END_STONE), world, random, chunkX, chunkZ);
                break;
            default:
                break;
        }
    }

    private void runGeneration(IBlockState blockToGen, int blockAmount, int chancesToSpawn, int minHeight, int maxHeight, Predicate<IBlockState> blockToReplace, World world, Random rand, int chunk_X, int chunk_Z) {
        if (minHeight < 0 || maxHeight > 256 || minHeight > maxHeight)
            throw new IllegalArgumentException("Illegal Height Arguments for LakRaces WorldGenerator");

        WorldGenMinable generator = new WorldGenMinable(blockToGen, blockAmount, blockToReplace);
        int heightDifference = maxHeight - minHeight + 1;
        for (int i=0; i<chancesToSpawn; i++){
            int x = chunk_X * 16 + rand.nextInt(16);
            int y = minHeight + rand.nextInt(heightDifference);
            int z = chunk_Z * 16 + rand.nextInt(16);

            generator.generate(world, rand, new BlockPos(x, y, z));
        }
    }
}
