package com.zephaniahnoah.nexusraces.commands;

import com.zephaniahnoah.nexusraces.PlayerData;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.management.PlayerList;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class SetClass extends CommandBase {
	
	public SetClass() {}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "setclass";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "/setclass <player> <class>";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		if(args.length < 2) {
			sender.sendMessage(new TextComponentString("Try "+getUsage(sender)));
			return;
		}
		PlayerList players = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList();
		if(sender instanceof EntityPlayerMP) {
			EntityPlayerMP p = (EntityPlayerMP)sender;
			if(players.getOppedPlayers().getEntry(p.getGameProfile()) == null) {
				sender.sendMessage(new TextComponentString("Hay! You don't have permission to use this command."));
				return;
			}
		}
		EntityPlayerMP target = players.getPlayerByUsername(args[0]);
		if(target==null) {
			sender.sendMessage(new TextComponentString("That player doesn't exist."));
		} else {
			String race = args[1].toLowerCase();
			StringBuilder builder = new StringBuilder(race);
			builder.setCharAt(0, String.valueOf(race.charAt(0)).toUpperCase().toCharArray()[0]);
			PlayerData.setClass(target, PlayerData.stringToClass(builder.toString()));
		}
	}
}
