package com.zephaniahnoah.nexusraces;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import electroblob.wizardry.registry.WizardryItems;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.config.Configuration;

public class Config {

	public static final String[][] classDescriptions = new String[11][];
	public static final String catagoryClassDescriptions = "Class Descriptions";
	public static final String catagoryGeneral = "General";
	public static List<ItemStack> books = new ArrayList<ItemStack>();

    public Config() { }

    private static Configuration config;

    public static void init(File file) {
        config = new Configuration(file);
        try {
            config.load();

            for(String s : config.getStringList("books", catagoryGeneral, new String[] {"1","2","3","4","5","6","7","8","9"}, "These are the books that Elves choose from when they start. Stored as damage values because that's how EBW works.")) {
            	books.add(new ItemStack(WizardryItems.spell_book,1,Integer.valueOf(s)));
            }

        	classDescriptions[0] = config.getStringList(PlayerClass.Human.name(), catagoryClassDescriptions, new String[] {	"Humans:",// TODO: Load these from the config. And make these the default
								"Jack of all trades, master of none.",
								"",
								"Humans have no default effects or",
								"abilities."}, null);

			classDescriptions[1] = config.getStringList(PlayerClass.Elf.name(), catagoryClassDescriptions, new String[] {	"Elves:",
								"People of mana and magic, your attempts",
								"at spellcasting will be better than most",
								"from the get-go! try not to take out",
								"your enemies head on, but from a",
								"distance with your arcane gifts!",
								"",
								"Better luck when enchanting. Fortune 1",
								"when mining lapis lazuli. Permanent",
								"weakness to melee attacks. Strength",
								"boost when using a bow. On startup, you",
								"can pick out 5 spell books to start",
								"casting your magic!"}, null);

			classDescriptions[2] = config.getStringList(PlayerClass.Dwarf.name(), catagoryClassDescriptions, new String[] {	"Dwarves:",
								"You are a dwarf that likes digging a",
								"hole, diggy diggy hole! As such, you'll",
								"find much fortune underground. Though",
								"keep in mind, you're a bit bulkier than",
								"the other races.",
								"",
								"Permanent slowness 1.",
								"Permanent haste 1.",
								"When health and hunger are both full,",
								"you'll gain fortune 1."}, null);

			classDescriptions[3] = config.getStringList(PlayerClass.Testificate.name(), catagoryClassDescriptions, new String[] {	"Testificates:",
								"The pen is mightier than the sword!",
								"As such your luck and fortune will save",
								"you more often than your fighting",
								"prowess.",
								"",
								"Luck 1 when health is full.",
								"Permanent weakness 2."}, null);

			classDescriptions[4] = config.getStringList(PlayerClass.Kalot.name(), catagoryClassDescriptions, new String[] {	"Kalots:", // Kaloti
								"The people of the hive thrive together!",
								"And traversing the land will be much",
								"easier for you than the other races!",
								"",
								"You are effected by the bane of",
								"arthropods enchantment. You get",
								"double drops from arthropods.",
								"Permanent feather falling. Permanent",
								"jump boost 1. Immunity to food",
								"poisoning. Speed boost 1 when exposed",
								"to daylight."
								}, null);

			classDescriptions[5] = config.getStringList(PlayerClass.Tekkin.name(), catagoryClassDescriptions, new String[] {"Tekkins:",
								"Your powers with redstone go",
								"unmatched! And your infinite",
								"technological knowledge grants you far",
								"better rewards for using your brain!",
								"",
								"When crafting redstone materials, get",
								"double crafting output! Weakness and",
								"slowness 1 when submerged in water or",
								"exposed to the rain. Permanent fortune",
								"when mining redstone and nether",
								"quartz."}, null);

			classDescriptions[6] = config.getStringList(PlayerClass.Awakened.name(), catagoryClassDescriptions, new String[] {	"The Awakened:",
								"As an undead with consciousness,",
								"you'll thrive at night or underground",
								"",
								"Burn in sunlight. Hostile mobs are",
								"neutral to you. Permanent night vision.",
								"Effected by the smite enchant. Health",
								"potions will harm you. Damage potions",
								"will heal you. Immune to food poisoning."}, null);

			classDescriptions[7] = config.getStringList(PlayerClass.Gaian.name(), catagoryClassDescriptions, new String[] {	"Gaians:",
								"You're a being of nature, and nature",
								"can be very polarizing. you'll have to",
								"think differently depending on the time",
								"of day.",
								"",
								"You will permanently have a weakness to",
								"poison and fire, however; When exposed",
								"to daylight: +regeneration, health boost,",
								"resistance, and slowness. When exposed",
								"to moonlight: +strength, speed boost,",
								"night vision, and weakness to",
								"projectiles and melee attacks."}, null);

			classDescriptions[8] = config.getStringList(PlayerClass.Lupithian.name(), catagoryClassDescriptions, new String[] {	"Lupithians:",
								"Children of the moon; your wolf blood",
								"will grant you great blessings when",
								"under the power of moonlight! but the",
								"sun will make you a bit more sluggish.",
								"",
								"Speed boost 1 and strength 1 when",
								"exposed to moonlight. Speed boost 2,",
								"strength 2, looting 1 when exposed to",
								"the full moon. Weakness 1 when exposed",
								"to daylight. When taming wolves, only 1",
								"bone is ever required. Take extra",
								"damage from fire."}, null);

			classDescriptions[9] = config.getStringList(PlayerClass.HellBorne.name(), catagoryClassDescriptions, new String[] {	"Hell Borne:",
								"People of the Nether, children of fire",
								"and brimstone! You're fully adapted to",
								"life in the hell scape you were born in!",
								"The Overworld however, is a different",
								"story.",
								"",
								"Permanent fire resistance. Immune to",
								"food poisoning. Fortune 1, Strength 1",
								"and resistance 1 when in the Nether.",
								"Drown twice as fast in the overworld.",
								"Weakness 1 when exposed to sunlight."}, null);

			classDescriptions[10] = config.getStringList(PlayerClass.Reezlan.name(), catagoryClassDescriptions, new String[] {	"Reezlans:",
								"Yo ho ho and a bottle of rum! The",
								"lizard-like Reezlans are masters of the",
								"seas, and pillage and plunder their way",
								"to victory!",
								"",
								"When submerged under water, gain",
								"water breathing for 2 minuets. Health",
								"boost 2 when submerged underwater.",
								"Permanent aqua affinity. Haste 1 when",
								"submerged underwater. Slowness 1",
								"when walking on land."}, null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            config.save();
        }
    }
}